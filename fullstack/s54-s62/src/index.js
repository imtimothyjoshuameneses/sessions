import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// Bootstrap Import
import 'bootstrap/dist/css/bootstrap.min.css';

// This is where REACT Attaches the component to the root element in the index.html file
// Strict mode allows react to be able to display and handle any error/warning that may occur
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
