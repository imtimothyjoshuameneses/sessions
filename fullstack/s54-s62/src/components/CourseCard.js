import {Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from 'react';

export default function CourseCard({course}){
	// Destructuring the contents of 'course'
	const {name, description, price} = course;

	// A state is just like a variable bit with the concept of getters and setters. The getter is responsible for retrieving the current value of the state, while the setter is responsible for modifying the current value of the state. The useState() hook is responsible for setting the initial value of the state
	const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(30);

	function enroll() {
		// The setCounr function can use the previous value fo the state and add/modify to it.

		if (seatCount === 0) {
			alert('No more Seats available.');
		} 
		else {
		setCount(prev_value => prev_value + 1 );
		setSeatCount(prev_value => prev_value - 1);
		}
	}

	// S55 Activity 
	// 1. Create a 'seats' state that will have an initial value of 30
	// 2. Create a functionality wherein when you click the 'enroll' button, the 'seats' state will go down  in value by 1 as well.
	// 3. Once the 'seats' state reaches 0 in value, show an alert that says 'No more seats available'.
	// 4. Push to gitlab once done.

	return(
		<Card>
		    <Card.Body>
		        <Card.Title>{name}</Card.Title>

		        <Card.Subtitle> Description: </Card.Subtitle>
		        <Card.Text> {description} </Card.Text>
		        <Card.Subtitle> Price: </Card.Subtitle>
		        <Card.Text> PHP {price} </Card.Text> 

		        <Card.Subtitle> Enrollees: </Card.Subtitle>
		        <Card.Text> {count} </Card.Text> 

		        <Card.Subtitle> Seats: </Card.Subtitle>
		        <Card.Text> {seatCount} </Card.Text> 



		        <Button variant="primary" onClick={enroll}>Enroll Now</Button>
		    </Card.Body>
		</Card>
				
	)
}

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}