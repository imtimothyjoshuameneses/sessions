// console.log("Love, Objects");

// [SECTION] Objects
/*

	- an object is a data type.
	- create properties and methods/Fucntionalities

*/

// creating objects using initializers/ objects literals

let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating objects using initializers/object literals");
console.log(cellphone);
console.log(typeof cellphone);

// Creating an object using a constructor function

function Laptop(name, manufactureDate){

	this.name = name;
	this.manufactureDate = manufactureDate;
}
// Multiple instance of an object using the "new" keyword
//  This method is called instantation
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using constructor function: ");
console.log(laptop);

// Another Example of instantation

let laptop2 = new Laptop("Macbook", 2020);
console.log("Result from creating objects using constructor function: ");
console.log(laptop2);


// [SECTION] Accessing Object Properties.

// using square bracket notation
console.log("Result from Square bracket notation " + laptop2["name"]);


// Using dot notation
console.log("Result from dot notation notation " + laptop.name);


// Access array objects.
let array = [laptop, laptop2];

console.log(array[0]["name"]);
console.log(array[0].name);

// [SECTION] Adding/Deleting/Reassigning Object Properties.

// Empty Object
let car ={};
console.log("Object before adding a properties ");
console.log(car);

//  Empty Object using ocnstructor function/ Instation 
let myCar = new Object();
console.log("Result of creating empty object using constructor function ");
console.log(myCar);






// Adding object properties using dot notation.
// objectName.kry = "value"
car.name = "Honda Civic"
console.log("Result from adding properties using do notation");
console.log(car)



// Adding object properties using square bracket notation.
car["manufacturing date"] = 2019;
console.log(car["manufacturing date"]);
console.log(car['Manufacturing Date']);
//  We cannot access the object property using dot notation if the key has spaces.
// console.log(car.manufactureDate);

console.log("Result from adding propeeties using the square bracket notation");
console.log(car);


// Deleting Object properties
delete car["manufacturing date"];
console.log("Result from deleting properties: ");
console.log(car);


// Reassigning Object Properties 
car.names = "Honda Civic Type R";
console.log("Result form reassigning properties");
console.log(car);






// [SECTION] Object Methods
/*
	- a methos is a function which acts as a property of an object. 
*/

let person = {
	name: "Barbie",
	greet: function() {
		// console.log("Hello! May name is " + person.name);
		console.log("Hello! May name is " + this.name);
	}
}
console.log(person);
console.log("This is the result from object methods");
// greet() is now called a method.
person.greet();




// Adding methods to objects
person.walk = function() {
	console.log(this.name + " walked 25 steps forward")
}
person.walk();


let friend = {
	// Values can be strings, numbers, arrays, b
	name: "ken",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ["ken@gmail.com", "ken@mail.com"],
	introduce: function(person) {
		console.log("Nice to meet you " + this.name + " I am " + this.name + " from " + this.address.city + " " + this.address.state);
	}
}

friend.introduce(person);