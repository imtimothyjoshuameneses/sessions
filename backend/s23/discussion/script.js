// [SECTION] If-else Statements
let number = 1;

if(number > 1){
	console.log("The number is greater than 1!");
} 
else if(number < 1){
	console.log("The number is less than 1!");
}
else{
	console.log("None of the conditions were true :(");
}


// Falsey values
if(false){
	console.log("falsey");
}

if(0){
	console.log("falsey");
}

if(undefined){
	console.log("falsey");
}

// Truthy values
if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}

// Ternary Operators
let result = (1 < 10) ? true : false;


//  if there are multiple lines within the if-else block, it's better to use the regular if-else syntax as the ternary operator is only capable of handling one-liner
if(5==5){
	let greeting = "hello";
	console.log(greeting);
}

console.log("Data returned form the ternary operator is " + result);

// [SECTION] Switch Statement
let day = prompt("What day of the week is it today?").toLowerCase();

switch(day){
	case 'monday':
	console.log("The day today is Monday!") 
	break;
	case 'tuesday':
	console.log("The day today is tuesday!") 
	break;
	case 'wednesday':
	console.log("The day today is wednesday!") 
	break;
	case 'thursday':
	console.log("The day today is thursday!") 
	break;
	case 'friday':
	console.log("The day today is friday!") 
	break;
	default:
		console.log("Please input a valid day naman paareh");
}

// [SECTION] Try/Catch/Finally Statement
function showIntensityAlert(windSpeed){
	try{
		alerat(determineTyphoonIntensity(windSpeed));
	}
	catch(error){
		console.log(error.message)
	}
	finally{
		alert("Intensity updates wil show alert!");
	}
}
showIntensityAlert(56);
