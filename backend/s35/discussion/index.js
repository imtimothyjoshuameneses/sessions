// Greater than operator
db.users.find({
  age: {
    $gt: 20,
  },
});



//Less than operator
db.users.find({
  age: {
    $lte: 82,
  },
});

// Regex operator
db.users.find({
  firstName: {
    $regex: "s",
    $options: "i",
  },
});

db.users.find({
  lastName: {
    $regex: "T",
    $options: "i",
  },
});

// Combing operators
db.users.find({
  age: {
    $gt: 70,
  },
  lastName: {
    $regex: "g",
  },
});

db.users.find({
  age: {
    $lte: 76,
  },
  firstName: {
    $regex: "j",
    $options: "i",
  },
});

// Field projection
db.users.find(
  {},
  {
    _id: 0,
  }
);

db.users.find(
  {},
  {
    firstName: 1,
    _id: 0, // exclude the id field from results
  }
);