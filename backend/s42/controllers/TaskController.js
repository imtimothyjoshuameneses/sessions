const Task = require('../models/Task.js');


module.exports.getAllTasks = () => {
	// Accessing that 'Task' model and using find() with an empty object as
	return Task.find({}).then((result, error) => {
		if(error){
			return {
				message: error.message
			}
		}

		return {
			tasks: result
		}
	})
}


module.exports.createTask = (requestBody) => {
	return Task.findOne({name: requestBody.name}).then((result, error) => {
		// Check if the task already exists by utilizing tha 'name' property. If it does, then return a response to the user. 
		if(result != null && result.name == requestBody.name) {
			return {
				message: 'Duplicate Task found!'
			}

		} else{
			// 1. Create a new instance of the task model which will contain that properties required based in the schema.
			let newTask = Task({
				name: requestBody.name
			});

			// 2. Save a new Task to the database
			return newTask.save().then((savedTask, error) => {
				if(error){
					return {
						message: error.message
					};
				}
				return {
					message: 'New Task Created!'
				}
			})
		}
	})
}