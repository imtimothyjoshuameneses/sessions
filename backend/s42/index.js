// Server Variables
const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config() //Initialization of the dotenv package
const taskRoutes = require('./routes/taskRoutes.js');
const app = express();
const port = 4000;


// MongoDB Connection 
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@b303-meneses.avmaxx6.mongodb.net/b303-todo?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let database = mongoose.connection;

database.on('error', () => console.log('connection error :('));
database.once('open', () => console.log('Connected to MongoDB!'));


// Middleware
app.use(express.json())
app.use(express.urlencoded({extended: true}));
app.use('/api/tasks' , taskRoutes); // initializing the routes for '/tasks' so the server will know the routes avaialable to send requests to.

// IF we send a request to the following URLs:
// http://Localhost:4000/api/tasks/ GET Method will access the 'Get all tasks'
// http://Localhost:4000/api/tasks/ POST Method will access the 'Create new tasks'

// Server Listening
app.listen(port, () => console.log(`Server is running at localhost: ${port}`));

module.exports = app;

