// [LESSON] Array Traversal

// [SECTION] Arrays and indexes

let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujistu", "Lenovo"];
let mixed_array = [12,"Asus", null, undefined, {}];

// Alternative way to write arrays
let my_tasks = [
	"drink html",
	"eat javascript",
	"inhale CSS",
	"bake sass"
];

// Reassign values
console.log("Array before reassignment");
console.log(my_tasks);

my_tasks[0] = "run hello world"; 
// To reassign a value in an array, just ise its index number and use an assignment operator to replace the value of that index
console.log("Array after reassignment: ");
console.log(my_tasks);


// Reading from arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an array
console.log(computer_brands.length);

// Accessing last element in an array
let index_of_last_element = computer_brands.length - 1;

console.log(computer_brands[index_of_last_element]);

// [SECTION] Array methods/ Array functions
let fruits = ["Apple", "orange", "Kiwi", "Passionfruit"];
console.log("Current Array: ")
console.log(fruits);

// Push method
fruits.push("Mango", "Cocomelon");

console.log("Updated array after push method: ")
console.log(fruits);

// Pop method
console.log("Current Array:")
console.log(fruits);

let removed_item = fruits.pop();

console.log("Updated array after pop method: ")
console.log(fruits);
console.log("Removed fruits: " + removed_item);


// Unshift Method - Adding Items in the beginning of the array
console.log("Current Array:")
console.log(fruits);

fruits.unshift("Lime", "Star apple");

console.log("Updated array after unshift method: ")
console.log(fruits);

// shift Method - Remove the items at the begining of the array
console.log("Current Array:")
console.log(fruits);

fruits.shift();

console.log("Updated array after shift method: ")
console.log(fruits);

// Splice Method 
console.log("Current Array:")
console.log(fruits);

// splice(startingIndex, numberOfItemsToBeDeleted, itemsToBeAdded)
fruits.splice(1, 3, 'lime', 'Cherry', 'kiwi');

console.log("Updated array after splice method: ")
console.log(fruits);

// Sort Method
console.log("Current Array:")
console.log(fruits);

fruits.sort();

console.log("Updated array after sort method: ")
console.log(fruits);


// [Sub-section] Non-Mutator Methods - Every methid written above is called a 'Mutator Method' because it modifies the value of the array one way or another

// indexOf method - gets the index of a specific item
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of Lenovo is: " + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is: " + index_of_lenovo_from_last_item);

// Slice Method 
let hobbies = ["Gaming", "Running", "Cheating", "Cycling", "Writing"];

// By putting '2' as the argument, we are starting the slicing process from the item with the index of 2
let slice_array_from_hobbies = hobbies.slice(2);
console.log("Hobbies after slice method: ");
console.log(slice_array_from_hobbies);
console.log(hobbies);

// By putting two arguments instead of one, we are also specifying where the slice will end 
let slice_array_from_hobbies_B = hobbies.slice(3,4);
console.log(slice_array_from_hobbies_B);
console.log(hobbies);

// By using a negative number as the index, the count where it start with with will come from the end of the array instead of the beginning. 
let slice_array_from_hobbies_C = hobbies.slice(-3);
console.log(slice_array_from_hobbies_C);
console.log(hobbies);

// toString Method 
let string_array = hobbies.toString();
console.log(string_array);

// Concat Method
let greeting = ["Hello", "World"];
let excalmation = ["!", "?"];

let concat_greeting = greeting.concat(excalmation);
console.log(concat_greeting);



// Join method - This will convert the array to a string AND insert a specified separator between them. The separator is defined as the argument of the join() funciton.
console.log(hobbies.join(' - '));


// foreach Method 
// for(let index = 5, index <=, )
hobbies.forEach(function(hobby){
	console.log(hobby);
})

// Map Method - Loop throughout the whole array and adds each item to a new array.
let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function(number){
	return number * 2;
})

console.log(numbers_map);


// Filter Method
let filtered_numbers = numbers_list.filter(function(number){
	return (number < 3);
})

console.log(filtered_numbers);

// [SECTION] Multidimentional Arrays

let chess_board = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chess_board[6][5]);