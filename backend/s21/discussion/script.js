// FUNCTION DECLARATION AND INVOCATIONS

// Declaration
function printName(){
	console.log("My name is Jeff");
}

printName(); //Invocation


// FUNCTION EXPRESSION
let variable_function = function(){
	console.log("Hello from function Expression!")
}

variable_function();


// SCOPING
let global_variable = "Call me Mr. Worldwide!";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);

	//  You can use global variable inside any functions as long as they are declared outside of the function scope.
	console.log(global_variable);
}

// You cannot use locally-scoped variables outside the function they are declared to. 

showNames();

// NESTED FUNCTIONS
function parentFunction() {
	let name = "Jane";

	function childFunction() {
		let nested_name = "John"

		console.log(name);

		// Accessing the nested_name variable within the same funcyion it was declared in, WILL work.
		console.log(nested_name);
	}

	childFunction();

	// Accessing the nested_name variable OUTSIDE the function it was declared in, it WILL not work.
	// console.log(nested_name); //will result to an error. 
}

parentFunction();

// BEST PRACTICE FOR FUNCTION NAMING
// function printWelcomeMessageForUser(){
// 	let first_name = prompt("Enter your first name: ");
// 	let last_name = prompt("Enter your last name: ");

// 	console.log("Hello, " + first_name + " " + last_name + "!");
// 	console.log("Welcome sa Page ko!");
// }

// printWelcomeMessageForUser();

// RETURN STATEMENT 
function fullName(){
	return "Timo Sesenem";
}

console.log(fullName());