// console.log("ES6 UPDATES");


// Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8,2);
console.log(secondNum);

// Templates literals
/*

	-Allows us to writes strings w/o using concatenation
	-greatly helps us with code readability 

*/

let name = "ken";

// using concatenation
let message = "Hello " + name + "! Welcome to programing";
console.log("Message without template literal: " + message);



// Using template literals
// Backticks (``) and ${} for including javascript expression
message = `Hello ${name}! Welcome to proramming`;
console.log(`Message with template literals: ${message}`)




// creates multi line using template literals.
const anotheMessage = `
${name} attended a Math Competition.
He won it by solving the problem 8 ** 2 with the answer of ${firstNum}`;
console.log(anotheMessage);



const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings amount is: ${principal * interestRate}`);

// Array Destructing
const	fullName = ["Juan", "Dela", "Cruz"];

// Using Array indeces
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

// Using Destructuring 
const	[firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

// Object Destructuring 
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}

// Using the dot notation 
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

// Using object destructuring 
const {givenName, familyName, maidenName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName} It's good to see you.`);

function getFullName({givenName, maidenName,familyName}) {

	console.log(`${givenName} ${maidenName} ${familyName}`)
};
getFullName(person);


// Arrow function
const hello = () => {

	console.log("Hello World!")
}

hello();

// Traditional function
/*function printFullName(fName,mName,lName) {
	console.log(fName + " " + mName + " " + lName);
}
printFullName('John', 'D', 'Smith');*/


// Arrow function with template literals 
const printFullName = (fName, mName, lName) => {

	console.log(`${fName} ${mName} ${lName}`)
}

printFullName('Jane', 'D', 'Smith');

// Arrow Functions with loops
const students = ['John', 'Jane', 'Judy']

// Traditional Function 
students.forEach(function(student) {

	console.log(`${student} is a student.`)
})

// Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})

// Implicit Return Statements

// Traditional Function
/*function add (x,y) {
	return x + y;
}

let total = add(2,5);
console.log(total);*/

// Arrow Function
// Single line arrow functions
const add = (x,y) => x + y;

let total = add(2,5);
console.log(total);

// Default Function Argument Values
// Provides a default argument
const greet = (name = 'User') => {
	return `Good Afternoon ${name}`
}

console.log(greet());
console.log(greet('Judy'));

// [SECTION] Class-based Object Blueprints

// Create a class
class Car {
	constructor(brand, name, year) {
		this.brand =  brand;
		this.name = name;
		this.year = year;
	}
}

// Instantiate an Object 
const fordExplorer = new Car();

// Even though the 'fordExplorer' object is const, since it is an object, you amys till reassign values to its properties.
fordExplorer.brand = "Ford";
fordExplorer.name = "Explorer";
fordExplorer.year = "2022";

console.log(fordExplorer);

// This logic applies wether you re-assign the values of each property separately or put it as arguments of the new instance of the class. 
const toyotaVios = new Car("Toyota", "Vios", 2018);

console.log(toyotaVios);
