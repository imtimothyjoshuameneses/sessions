let http = require("http");

// Create a variable port to store the port number: 4000
const port = 4000;

// create a variable app that stores the output of the createServer() method
// This allows us to use the http createServer's other method
const app = http.createServer((req, res) => {
 	
 	// req is an object that I sent via browser.
 	// url is a property that refers to the url or link in the browser
	if(req.url == '/greeting') {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end("Hello again");
		
	} else if(req.url == '/homepage') {
		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end("Welcome to the Homepage.");

	// All other routes thata re not included in if else-if
	} else {

		res.writeHead(404, {'Content-Type': 'text/plain'});

		res.end("error 404: Page not found");
	}
})

app.listen(port);

console.log(`Server now accessible at localhost:${port}.`);