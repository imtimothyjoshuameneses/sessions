// SERVER Variables for initialization
const express = require('express'); //import express for the project
const app = express(); // Invoke express to be use for the whole project
const port = 4000; 


// Middleware
app.use(express.json()); // Responsible for converting every json format into regular javascript
app.use(express.urlencoded({extended: true})); // Resposible for accepting extension for the project that will allow express to be able to read data types other than the deafault string and array it can usually read.


// Server Listening 
app.listen(port, () => console.log(`Server is running at port ${port}`)); 

// [SECTION] Routes 
app.get('/', (request, response) => {
	response.send('Hello World!');
})

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`);
})

// Mock Database
let users = [];

app.post('/register', (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} has succesfully been registered!`);
	} else {
		response.send('Please input BOTH username AND password.');
	}
})

// Gets the list/ array of users
app.get('/users', (request, response) => {
	response.send(user); 
})
module.exports = app;
