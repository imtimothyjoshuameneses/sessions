const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


// Create single course
module.exports.addCourse = (request_body) =>{
	let new_course = new Course({
		name: request_body.name,
		description: request_body.description,
		price: request_body.price
	});

	return new_course.save().then((saved_course, error) => {
		if(error){
			return {
				message: false
			};
		}
		return {
			message: true
		};
	}).catch(error => console.log(error));

}


// Get all courses
module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		return response.send(result);
	})
}


// Get all active courses
module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return response.send(result);
	})	
}


// Get Course
module.exports.getCourse = (request, response) => {
	return Course.findById(request.params.id).then(result => {
		return response.send(result);
	})
}


// update single course
module.exports.updateCourse = (request, response) => {
	let updated_course_details = {
		name: request.body.name,
		decription: request.body. description,
		price: request.body.price
	};

	return Course.findByIdAndUpdate(request.params.id, updated_course_details).then((course, error) => {

		if(error){
			return response.send({
				message: error.message
			})
		}
		return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}

// archive single course
module.exports.archiveCourse = (request, response) => {
	let archive_course = {
		isActive: request.body.isActive
	};

	return Course.findByIdAndUpdate(request.params.id, archive_course).then((course_archived, error) => {

		if(error) {
			return response.send({
				message: error.message
			})
		}
		return response.send(true)

	}).catch(error => console.log(error));
}


// activate course
module.exports.activateCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, {isActive: 'true'}, {new: true}).then((course_activated, error) => {

		if(error) {
			return response.send({
				message: error
			})
		}

		return response.send(true)

	}).catch(error => console.log(error));
}


// Search course by name
module.exports.searchCourses = (request, response) => {
    
      const courseName = request.body.courseName;
      
      return Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
      	response.send(courses)
      }).catch(error => response.send({
      		message: error.message
      }))
 
    }
  
