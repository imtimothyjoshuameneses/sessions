const mongoose = require('mongoose');

// Schema
const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required : [true, "First name is required"]
	},
	lastName: {
		type: String,
		required : [true, "Lastname is required"]
	},
	email:{
		type: String,
		required : [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: true
	},
	mobileNo: {
		type: String,
		required : [true, "MobileNo. is required"]
	},
	enrollments:[
		{	
			courseId: {
				type: String,
				required: [true, "CourseId is required"]
			},
			enrolledOn: {
				type : Date,
                default : new Date() 
            },
            status: {
            	type: String,
            	default: "Enrolled"
            }
		}
	]
})

module.exports = mongoose.model("User", user_schema);