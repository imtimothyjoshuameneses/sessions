const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js');
const auth = require('../auth.js');

// You can destructure the 'auth' variable to extract the function being exported from it. You canthe use the functions directly without having to use dot(.) notation
// const {verify, verifyAdmin} = auth;

// Insert routes here

// Create single Course
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request.body).then((result) => {
		response.send(result);
	})
})


// Get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
})


// Get all active courses
router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
})


// Get a specific course
router.get('/:id', (request, response) => {
	CourseController.getCourse(request, response);
})


// Update single course
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})

// Archive Course
router.put('/:id/archive', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.archiveCourse(request, response);
})



// Activate Course
router.put('/:id/activate', auth.verify, auth.verifyAdmin,(request, response) => {
	CourseController.activateCourse(request, response);
})

// Search course by name
router.post('/search', (request, response) => {
	CourseController.searchCourses(request, response);
});

module.exports = router;