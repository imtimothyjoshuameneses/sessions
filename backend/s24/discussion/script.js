// [SECTION] WHILE-LOOP
let count = 5;

while(count !== 0){
	console.log("Current Value of count " + count);
	count--;
}

// [SECTION] DO-WHILE LOOP
let number = Number(prompt("Enter a number: "));

do{
	console.log("Current Value of number: " + number);
	number += 1;
}
while(number < 10);


// [SECTION]	FOR LOOP
for(let count = 0; count <=20; count++){
	console.log("current for loop value: " + count);
}

let my_string = "Timothy";

// To get a length of a string
console.log(my_string.length);

// To get specific letter in a string
console.log(my_string[0]);

// Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
for(let index = 0; index < my_string.length; index++){
	console.log(my_string[index]);
}

MINI ACTIVITY (20 mins).

let my_name = "Timothy Joshua Meneses";

for(let index = 0; index < my_name.length; index++){
	if(
		my_name[index].toLowerCase() == 'a' ||
		my_name[index].toLowerCase() == 'e'	||
		my_name[index].toLowerCase() == 'i' ||
		my_name[index].toLowerCase() == 'o' ||
		my_name[index].toLowerCase() == 'u' 
	){
		// console.log(my_name[index]);
		continue;

	} else{
		console.log(my_name[index]);
	}
	
	
}




let name_two = "Joshua";

for(let index = 0; index < name_two.length; index++){
	
	// if the current letter is 'a' then it will reitereate the loop
	if(name_two[index].toLowerCase() == "a"){
		console.log("Skipping...");
		continue;
	}
	// if the current letter is 'e' then it will break/stop the whole loop entirely
	if(name_two[index] == "e"){
		break;
	}
	console.log(name_two[index]);
}
