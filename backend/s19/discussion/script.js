// alert("Hello World!");

console.log("Hello World!");


// [SECTIONS] variables


// Varibale Declaration & Invocation
let	my_variable = "Hola Mundo!";
console.log(my_variable);

// Concatenating Strings
let country = "Philippines";
let province = "Metro Manila";

let full_address = province + "," + country;

console.log(full_address);

// Numbers/Integers 
let headcount = 26;
let grade = 98.7;

console.log("The number of students is " + headcount + " and the average grade of all students is " + grade);

// Adding numbers/ integers 
// let sum = headcount + grade;

// console.log(sum);

// Boolean - The value of boolean is ONLY true or false. When naming a varaibles that have a bollean value, make sure they're formatted like a questions.
let is_married = false;
let is_good_conduct = true;

console.log("He's Married: " + is_married);
console.log("She's a good person: " + is_good_conduct);

// Arrays 
let grades = [98.7, 89.9, 90.2, 94.6];
let details = ["John", "Smith", 32, true];

console.log(details);

// Objects 
let person = {
	fullName: "Juan Dela Cruz",
	age: 40,
	isMarried: false,
	contact: ["090002223333", "09448881028"],
	address: {
		house_number: "345",
		city: "England"
	}
}

// Javascript reads arrays as objects. This is mainly to accomodate for specific functionalities that array can do later on. 
console.log(person);
console.log(grades);

// Null & Undefined
let girlfriend = null;
let full_name; 

console.log(girlfriend);
console.log(full_name);

// [SECTION] Operators 

// Arithmetic operators 
let first_number = 5;
let second_number = 5;

let sum = first_number + second_number;
let difference = first_number - second_number;
let product = first_number * second_number;
let quotient = first_number / second_number;
let remainder = first_number % second_number;

console.log("Sum " + sum);
console.log("Difference " + difference);
console.log("Product " + product);
console.log("Quotient " + quotient);
console.log("Remainder " + remainder);

// Assignment Operators 
let assignment_number = 0; // initialize zero as the assignment

assignment_number = assignment_number + 2;
console.log("Result of addition assignment operator: " + assignment_number);

assignment_number += 2;
console.log("Result of shorthand addition assignment operator: " + assignment_number );